from academy.task_collection import Task
from pybpodapi.protocol import Bpod
from academy import utils, settings


class TouchTeaching(Task):
    def __init__(self):
        super().__init__()

        self.info = """
        # TASK INFO
        Animals crossing the corridor triggers response window. Screen touches during resp win deliver reward.

        # PORTS INFO
        Port 1 - WATER PORT: LED, photogates and pump
        Port 2 - BUZZER: valve (16kHz): correct; LED (4kHz):punish
        Port 3 - Corridor photogates 1
        Port 4 - Corridor photogates 2, Global LED
        Serial 1 - MOTOR: Arduino
        """

    def init_variables(self):

        # general
        self.duration_min = 1800  # 30 mins
        self.duration_max = 2400  # 40 mins
        self.response_duration = 120 # 2 min
        self.mask = 3

        # fixations
        self.fixation_time = 1

        # screen
        self.stim_duration = self.response_duration
        self.x = 250
        self.y = 190
        self.width = 500 #all screen
        self.correct_th = settings.WIN_SIZE[0] * 2
        self.repoke_th = settings.WIN_SIZE[0] * 2


        # pumps
        self.valve_time = utils.water_calibration.read_last_value('port', 1).pulse_duration
        self.valve_reward = utils.water_calibration.read_last_value('port', 1).water

        # counters
        self.reward_drunk = 0


    def configure_gui(self):
        pass

    def main_loop(self):
        print('')
        print('Trial: ' + str(self.current_trial))

        if self.current_trial == 0:  # check door is closed
            self.sma.add_state(
                state_name='Start_task',
                state_timer=0,
                state_change_conditions={Bpod.Events.Port4In: 'Real_start'},
                output_actions=[(Bpod.OutputChannels.Serial1, 11)]
            )
            self.sma.add_state(
                state_name='Real_start',  # close  corridor door 2 when subject enter to the behav box
                state_timer=0,
                state_change_conditions={Bpod.Events.Tup: 'Wait_for_fixation'},
                output_actions=[(Bpod.OutputChannels.SoftCode, 20)]
            )
        else:
            self.sma.add_state(
                state_name='Start_task',
                state_timer=0,
                state_change_conditions={Bpod.Events.Tup: 'Wait_for_fixation'},
                output_actions=[]
            )

        self.sma.add_state(
            state_name='Wait_for_fixation',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port3In: 'Fixation'},
            output_actions=[]
        )

        self.sma.add_state(
            state_name='Fixation',
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'Response_window'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 1)] #show 3 stimuli
        )
        self.sma.add_state(
            state_name='Response_window',
            state_timer=self.response_duration+10,
            state_change_conditions={'SoftCode1': 'Correct_first', 'SoftCode3': 'Miss', Bpod.Events.Tup: 'Miss'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 4)]
        )
        self.sma.add_state(
            state_name='Correct_first',
            state_timer=1,
            state_change_conditions={Bpod.Events.Port1In: 'Correct_first_reward'},
            output_actions=[(Bpod.OutputChannels.PWM1, 5), (Bpod.OutputChannels.Valve, 2),
                            (Bpod.OutputChannels.SoftCode, 11)] # waterLED and RWsound remain ON until poke
        )
        self.sma.add_state(
            state_name='Correct_first_reward',
            state_timer=self.valve_time * 1.5,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.Valve, 1), (Bpod.OutputChannels.SoftCode, 15)]
        )
        self.sma.add_state(
            state_name='Miss',
            state_timer=1,
            state_change_conditions={Bpod.Events.Port1In: 'Miss_reward'},
            output_actions=[(Bpod.OutputChannels.PWM1, 5), (Bpod.OutputChannels.LED, 4), (Bpod.OutputChannels.SoftCode, 12)]
            # waterLED ON, global LED ON
        )
        self.sma.add_state(
            state_name='Miss_reward',
            state_timer=self.valve_time * 0.5,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.Valve, 1), (Bpod.OutputChannels.SoftCode, 15)]
        )
        self.sma.add_state(
            state_name='Exit',  # Doors closure when trial ends
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'exit'},
            output_actions=[]
        )

    def after_trial(self):

        # Trial Counter
        if self.current_trial_states['Miss'][0][0] > 0:
            self.register_value('trial_result', 'miss')
            self.reward_drunk += self.valve_reward * 0.5
        else:
            self.register_value('trial_result', 'correct_first')
            self.reward_drunk += self.valve_reward

        # Relevant prints
        self.register_value('reward_drunk', self.reward_drunk)
        self.register_value('response_x', self.response_x)
        self.register_value('response_y', self.response_y)
        self.register_value('mask', self.mask)