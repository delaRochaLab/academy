from academy.task_collection import Task
from pybpodapi.protocol import Bpod


class Habituation(Task):

    def __init__(self):
        super().__init__()

        self.info = """
        HABITUATE ANIMALS 10 MINS.
        """

    def init_variables(self):
        self.trials_max = 4
        self.duration_min = 600  # 10 mins
        self.duration_max = 660

    def configure_gui(self):
        pass

    def main_loop(self):
        if self.current_trial == 0:  # check door is open
            self.sma.add_state(
                state_name='Start_task',  # check doors are opened and deliver 100 ul reward
                state_timer=self.valve_time,
                state_change_conditions={Bpod.Events.Tup: 'Start_task2'},
                output_actions=[(Bpod.OutputChannels.Serial1, 11), (Bpod.OutputChannels.Valve, 1),
                                (Bpod.OutputChannels.PWM4, 5)])
            self.sma.add_state(
                state_name='Start_task2',
                state_timer=0,
                state_change_conditions={Bpod.Events.Port4In: 'Real_start'},
                output_actions=[(Bpod.OutputChannels.PWM4, 5)])

            self.sma.add_state(
                state_name='Real_start',  # close corridor door 2 when subject enters to behavioral box
                state_timer=0,
                state_change_conditions={Bpod.Events.Tup: 'Habituation'},
                output_actions=[(Bpod.OutputChannels.SoftCode, 20), (Bpod.OutputChannels.PWM4, 5)])

        self.sma.add_state(
            state_name='Habituation',
            state_timer=self.duration_min / 4,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.PWM4, 5)])

        self.sma.add_state(
            state_name='Exit',
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'exit'},
            output_actions=[(Bpod.OutputChannels.PWM4, 5)])


    def after_trial(self):
        self.register_value('reward_drunk', self.reward_drunk)