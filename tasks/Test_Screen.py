from academy.task_collection import Task
from pybpodapi.protocol import Bpod
import numpy as np
import random
from user import settings

class Test_Screen(Task):

    def __init__(self):
        super().__init__()

        self.info = """
        Checking psychopy.
        """

    def init_variables(self):
        self.trials_max = 10

        self.wmi_stim_len = 0.3

        self.fixation_time = 1
        self.response_duration = 5
        self.delay = 0
        self.correct_th = settings.WIN_SIZE[0]
        self.repoke_th = settings.WIN_SIZE[0]

        self.x = 200  # center of the screen
        self.y = 190  # screen height is 250mmm
        self.width = 40

    def configure_gui(self):
        self.gui_input = ['trials_max', 'wmi_stim_len', 'width', 'delay']

    def main_loop(self):
        # Choose stimulus position (screen is 0-400 mm)
        # self.x = random.randint(20, 380)

        # Choose stimulus display time
        # stimulus lenghts:
        self.stim_duration_1 = self.response_duration + self.fixation_time
        self.stim_duration_2 = self.wmi_stim_len + self.fixation_time
        self.stim_duration_3 = self.fixation_time

        self.stim_duration = np.random.choice([self.stim_duration_2])

        if self.current_trial == 0:
            self.init_delay = self.delay

        if self.stim_duration == self.stim_duration_3: #delay trial
            print('Trial type: DS')
            self.delay = self.init_delay
        else:
            print('Trial type: WMI')
            self.delay = 0

        # Start the State Machine
        if self.current_trial == 0:  # check doors are closed
            self.sma.add_state(
                state_name='Start_task',  # check doors are closed
                state_timer=0,
                state_change_conditions={Bpod.Events.Tup: 'Wait_for_fixation'},
                output_actions=[(Bpod.OutputChannels.Serial1, 12)]
            )
        else:
            self.sma.add_state(
                state_name='Start_task',
                state_timer=0,
                state_change_conditions={Bpod.Events.Tup: 'Wait_for_fixation'},
                output_actions=[]
            )
        self.sma.add_state(
            state_name='Wait_for_fixation',
            state_timer=4,
            state_change_conditions={Bpod.Events.Tup: 'Fixation'},
            output_actions=[]
        )
        self.sma.add_state(
            state_name='Fixation',
            state_timer=self.fixation_time,
            state_change_conditions={Bpod.Events.Tup: 'Delay'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 1)]
        )
        self.sma.add_state(
            state_name='Delay',
            state_timer=self.delay,
            state_change_conditions={Bpod.Events.Tup: 'Doors'},
            output_actions=[]
        )
        self.sma.add_state(
            state_name='Doors',
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'Response_window'},
            output_actions=[(Bpod.OutputChannels.Serial1, 11)]
        )
        self.sma.add_state(
            state_name='Response_window',
            state_timer=1,
            state_change_conditions={'SoftCode3': 'Miss'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 4)]
        )
        self.sma.add_state(
            state_name='Miss',
            state_timer=2,
            state_change_conditions={Bpod.Events.Tup: 'exit'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 12), (Bpod.OutputChannels.Serial1, 12)])

    def after_trial(self):
        pass

