from academy.task_collection import Task
from pybpodapi.protocol import Bpod
from academy.utils import utils
from user import settings
import random
import numpy as np


class StageTraining_6B_V1(Task):

    def __init__(self):
        super().__init__()

        self.info = """
        # TASK INFO
        Stage 1: Stimulus categorization. 
        # Sb1: 70% VG 30% WMI, Repoking allowed; Sb2: 70% VG 30% WMI, Punish introduced; Sb3: 40% VG 60% WMI Consolidation.
        Stage 2: 20% VG 80% WMI. Progressively reduce stim duration in RW   
        Stage 3: 10% VG 45% WMI 45% WMD. Progressively reduce stim duration in RW       
        Stage 4: Data collection

        # PORTS INFO
        Port 1 - WATER PORT: LED, photogates and pump
        Port 2 - BUZZER: valve (16kHz): correct; LED (4kHz):punish
        Port 3 - Corridor PHOTOGATES 1
        Port 4 - Corridor PHOTOGATES 4, Global LEDs
        PA module 1 - Corridor PHOTOGATES 2
        PA module 2 - Corridor PHOTOGATES 3
        """


    def init_variables(self):

        # general
        self.duration_min = 2700 #45 min
        self.duration_max = 3600  # 1h
        self.stage = 6
        self.substage = 1
        self.mask = 3
        self.tired = False

        self.correction_bias = 0
        self.response_duration = 60

        # screen
        self.stim_duration = 0        # trial types probs
        self.pvg = 1
        self.pwm_i = 0
        self.pwm_ds = 0
        self.pwm_dm = 0
        self.pwm_dl = 0
        self.trial_type = 'VG'
        self.delay_type = np.nan


        self.delay = 0
        self.x = 0
        self.y = 125  # screen height is 250mmm
        self.width = 30
        self.correct_th = 130
        self.repoke_th = settings.WIN_SIZE[0] * 2
        self.punish_intro = 0.65

        self.stim_duration_vg = 0
        self.stim_duration_wm_i = 0
        self.stim_duration_wm_d = 0
        self.dl = 0
        self.rw_stim_dur = 0 #0.45
        self.wm_stim_dur = 0 #0.4

        # pumps
        self.valve_time = utils.water_calibration.read_last_value('port', 1).pulse_duration
        self.valve_reward = utils.water_calibration.read_last_value('port', 1).water
        self.valve_factor = 1
        self.valve_factor2 = 0.4

        # counters
        self.valid_counter = 0
        self.tried_counter = 0
        self.reward_drunk = 0
        self.running_window = 10
        self.accwindow = [0] * self.running_window  # Vector to store accuracy. It is continuously updated eliminating previous trials.
        self.vg_accwindow = [0] * self.running_window
        self.wmi_accwindow = [0] * self.running_window
        self.wmd_accwindow = [0] * self.running_window


    def configure_gui(self): # Variables that appear in the gui
        self.gui_input = ['stage', 'substage', 'rw_stim_dur', 'wm_stim_dur', 'duration_max']


    def main_loop(self):
        print('')
        print('Trial: ' + str(self.current_trial))

        # Register initial values
        if self.current_trial == 0:
            self.init_rw_stim_dur = self.rw_stim_dur
            self.init_wm_stim_dur = self.wm_stim_dur

        # Acuracy by trial type
        self.accuracy = sum(self.accwindow) / len(self.accwindow)
        self.vg_accuracy = sum(self.vg_accwindow) / len(self.vg_accwindow)
        self.wmi_accuracy = sum(self.wmi_accwindow) / len(self.wmi_accwindow)
        self.wmd_accuracy = sum(self.wmd_accwindow) / len(self.wmd_accwindow)

        print("Global Accuracy: " + str(self.accuracy))
        print("VG Accuracy: " + str(self.vg_accuracy))
        print("WMI Accuracy: " + str(self.wmi_accuracy))
        print("WMD Accuracy: " + str(self.wmd_accuracy))

        ############ VARIABLES BY STAGE ################

        ##### STAGE 1: STIMULUS CATEGORIZATION

        if self.stage == 1:
            # 5 first easy trials all VG
            if self.current_trial >= 5: # SUBSTAGE 1: STIMULUS REPOKING
                self.pvg = 0.7
                self.pwm_i = 0.3
                self.pwm_ds = 0
                self.rw_stim_dur = 0.45
                self.response_duration = 60

                if self.substage == 2 and self.current_trial >= 10: # SUBSTAGE 2: PUNISH INTRODUCTION
                    self.response_duration = 30
                    self.rw_stim_dur = 0.4
                    self.correction_bias = 1
                    # repoking allowed
                    if self.accuracy <= (self.punish_intro - 0.25) and self.current_trial % self.running_window == 0:
                        self.repoke_th = settings.WIN_SIZE[0] * 2
                        print('repoking allowed again')
                    # repoking not allowed
                    elif self.accuracy >= self.punish_intro and self.current_trial % self.running_window == 0:
                        self.repoke_th = self.correct_th
                        print('repoking not allowed')

                elif self.substage == 3 and self.current_trial >= 10: # SUBSTAGE 3: COLSOLIDATING
                    self.correction_bias = 1
                    self.response_duration = 30
                    self.rw_stim_dur = 0.35
                    self.repoke_th = self.correct_th
                    self.pvg = 0.4
                    self.pwm_i = 0.6


        else: # Other stages
            if self.current_trial <= 3:   # 3 easy trials: 100% VG, repoking allowed
                self.pvg = 1
                self.pwm_i = 0
                self.pwm_ds = 0
                self.response_duration = 60

            elif self.current_trial <= 7:  # 3 following intermediate trials
                self.pvg = 0.4
                self.pwm_i = 0.6
                self.response_duration = 30
                self.rw_stim_dur = (0.6+self.init_rw_stim_dur)/2

            elif self.current_trial  == 8:
                self.repoke_th = self.correct_th # no more repoking allowed
                self.rw_stim_dur = self.init_rw_stim_dur

                if self.mask == 3:
                    self.acc1_up = 0.6
                    self.acc1_down = 0.4
                    self.acc2_up = 0.5
                    self.acc2_down = 0.3
                elif self.mask == 5:
                    self.acc1_up = 0.6
                    self.acc1_down = 0.4
                    self.acc2_up = 0.45
                    self.acc2_down = 0.25


            ##### STAGE 2: WMI STIM DUR REDUCTION
            elif self.stage == 2:
                self.pvg = 0.2
                self.pwm_i = 0.8
                self.correction_bias = 1
                # from trial 20 start reducing RW stim dur
                if self.current_trial >= 20 and self.current_trial % self.running_window == 0:
                    if self.accuracy >= self.acc1_up and self.rw_stim_dur > 0.05:
                        self.rw_stim_dur -= 0.05
                        print('more difficult!')
                    elif self.accuracy <= self.acc1_down and self.rw_stim_dur < self.init_rw_stim_dur:
                        self.rw_stim_dur += 0.075
                        print('easier!')

            elif self.stage >= 3:
                self.response_duration = 15
                self.rw_stim_dur = 0
                self.pvg = 0.1
                self.pwm_i = 0.45
                self.pwm_ds = 0.45

                ##### STAGE 3: WMDS STIM DUR REDUCTION
                if self.stage == 3:
                    # from trial 20 start reducing wm_d stim duration
                    if self.current_trial >= 20 and self.current_trial % self.running_window == 0:
                        if self.wmd_accuracy >= self.acc2_up and self.wm_stim_dur > 0.05:
                            self.wm_stim_dur -= 0.05
                            print('more difficult!')
                        elif self.wmd_accuracy <= self.acc2_down and self.wm_stim_dur < self.init_wm_stim_dur:
                            self.wm_stim_dur += 0.075
                            print('easier!')

                elif self.stage >= 4:
                    self.wm_stim_dur = 0
                    self.rw_stim_dur = 0

                    ##### STAGE 4:
                    if self.stage == 4 and self.current_trial >= 12:
                        self.pwm_i = 0.35
                        self.pwm_ds = 0.35
                        self.pwm_dl = 0.2

                    ##### DATA COLLECTION:
                    elif self.stage == 6 and self.current_trial >= 12:
                        self.pwm_i = 0.3
                        self.pwm_ds = 0.3
                        self.pwm_dl = 0.3


        ############ CHANGING VARIABLES ################

        # Stimulus positions (screen is 0-400 mm)
        if self.mask == 3:  # 3 holes
            self.x_positions = [60, 200, 340] #65, 200, 335
        elif self.mask == 5:  # 5 holes
            self.x_positions = [24, 113, 200, 291, 380] #44, 123.5, 203, 282.5, 362 ajustar!

        # Choose positions
        if self.mask != 0:  # Mask
            if self.current_trial == 0: # Make a list with x values
                self.x_trials = random.choices(self.x_positions, k=1000)
                print('x positions list: ' + str(self.x_trials))
            # Choose x
            self.x = self.x_trials[self.current_trial]
            # Correction bias code
            if self.correction_bias == 1 and self.current_trial > 0:
                if self.trial_result == 'punish':
                    self.x = self.last_x
                    print('correction trial, x position:' + str(self.x))

        else:  # Full screen
            self.x = random.randint(20, 380)
        print('x position:' + str(self.x))


        #silent trials
        if self.current_trial >= 11 and self.stage==6:
            self.y = np.random.choice([125,1000], p=[0.95, 0.05]) #5% trials stimulus doesn't appear on the screen
            if self.y == 1000:
                print('SILENT TRIAL!')


        # Choose stimulus display time
        self.stim_duration_vg = self.response_duration
        self.stim_duration_wm_i = self.rw_stim_dur
        self.stim_duration_wm_d = self.wm_stim_dur

        self.pwm_d = self.pwm_ds + self.pwm_dm + self.pwm_dl
        sum_stim = self.pvg + self.pwm_i + self.pwm_d
        probs_stim = [self.pvg / sum_stim, self.pwm_i / sum_stim, self.pwm_d / sum_stim]
        self.trial_type = np.random.choice(['VG', 'WM_I', 'WM_D'], p=probs_stim)

        if self.trial_type == 'VG':
            self.stim_duration = self.stim_duration_vg
        elif self.trial_type == 'WM_I':
            self.stim_duration = self.stim_duration_wm_i
        elif self.trial_type == 'WM_D':
            self.stim_duration = self.stim_duration_wm_d

        print('correct_th: '+str(self.correct_th))
        print('repoke_th: ' + str(self.repoke_th))
        print('Trial type: ' + str(self.trial_type))
        print('[Prob VG, Prob WMI, Prob WMD]: ' + str(probs_stim))
        print('RW stim duration: ' + str(self.rw_stim_dur))
        print('WM stim duration: ' + str(self.wm_stim_dur))

        # Choose delay
        if self.trial_type == 'WM_D':
            probs_delay = [self.pwm_ds / self.pwm_d, self.pwm_dm / self.pwm_d, self.pwm_dl / self.pwm_d]
            self.delay_type = np.random.choice(['DS', 'DM', 'DL'], p=probs_delay)

            print('Delay type: ' + str(self.delay_type))
            print('[Prob DS, Prob DM, Prob DL]: ' + str(probs_delay))

        ############ STATE MACHINE ################

        ### change conditions depending on trial type
        if self.trial_type != 'WM_D':
            output_stim1=0
            output_stim2=0
            output_stim3=1
        else:
            if self.delay_type == 'DS':
                output_stim1 = 0
                output_stim2 = 1
                output_stim3 = 15
            elif self.delay_type == 'DL':
                output_stim1 = 1
                output_stim2 = 15
                output_stim3 = 15

        # Only the first trial
        if self.current_trial == 0:
            self.sma.add_state(
                state_name='Start_task', #show stim inifite time
                state_timer=0,
                state_change_conditions={'PA1_Port1In': 'Real_start'},
                output_actions=[(Bpod.OutputChannels.SoftCode, 2)]
            )
            self.sma.add_state(
                state_name='Real_start',  #close corridor 2 door when animal enter to behav box and deliver a samall amount of water
                state_timer=self.valve_time*0.5,
                state_change_conditions={Bpod.Events.Tup: 'Fixation1'},
                output_actions=[(Bpod.OutputChannels.SoftCode, 20), (Bpod.OutputChannels.Valve, 1)]
            )

        # Other trials
        else:
            self.sma.add_state(
                state_name='Start_task',
                state_timer=0,
                state_change_conditions={'PA1_Port1In': 'Fixation1', 'PA1_Port1Out':'Fixation1'},
                output_actions=[]
            )
        self.sma.add_state(
            state_name='Fixation1',
            state_timer=0,
            state_change_conditions={'PA1_Port2In': 'Fixation2'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 2)]  # show stimulus now
        )
        self.sma.add_state(
            state_name='Fixation2',
            state_timer=0,
            state_change_conditions={'PA1_Port3In': 'Fixation3'},
            output_actions=[(Bpod.OutputChannels.SoftCode, output_stim1)]  # show stimulus for specified time in WMDL
        )
        self.sma.add_state(
            state_name='Fixation3',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port3In: 'Pre_Response_window'},
            output_actions=[(Bpod.OutputChannels.SoftCode, output_stim2)]  # show stimulus for specified time in WMDS
        )
        self.sma.add_state(
            state_name='Pre_Response_window',
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'Response_window'},
            output_actions=[(Bpod.OutputChannels.SoftCode, output_stim3)]  # show stimulus for specified time in WMI & VG
        )

        self.sma.add_state(
            state_name='Response_window',
            state_timer=self.response_duration+10,
            state_change_conditions={'SoftCode1': 'Correct_first', 'SoftCode2': 'Incorrect', 'SoftCode3': 'Miss',
                                     'SoftCode4': 'Punish', Bpod.Events.Tup: 'Miss'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 4)]
        )
        self.sma.add_state(
            state_name='Correct_first',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port1In: 'Correct_first_reward'},
            output_actions=[(Bpod.OutputChannels.PWM1, 5), (Bpod.OutputChannels.Valve, 2),
                            (Bpod.OutputChannels.SoftCode, 11)]
            # waterLED and correct sound remain ON until poke
        )
        self.sma.add_state(
            state_name='Miss',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port1In: 'Miss_reward', Bpod.Events.Port4In: 'Miss_reward'},
            output_actions=[(Bpod.OutputChannels.PWM1, 5), (Bpod.OutputChannels.LED, 4), (Bpod.OutputChannels.SoftCode, 12)]
            # waterLED ON, global LEDs ON
        )
        self.sma.add_state(
            state_name='Punish',
            state_timer=1,
            state_change_conditions={Bpod.Events.Tup: 'After_punish'},
            output_actions=[(Bpod.OutputChannels.LED, 2), (Bpod.OutputChannels.LED, 4), (Bpod.OutputChannels.SoftCode, 14)]
            # Incorrect sound, global LEDs on
        )
        self.sma.add_state(
            state_name='After_punish',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port1In: 'Miss_reward', Bpod.Events.Port1Out: 'Miss_reward',
                                     Bpod.Events.Port4In: 'Miss_reward', Bpod.Events.Port4Out: 'Miss_reward'},
            output_actions=[(Bpod.OutputChannels.LED, 4), (Bpod.OutputChannels.PWM1, 5)]
            # water & global LED ON
        )
        self.sma.add_state(
            state_name='Incorrect',
            state_timer=0.25,
            state_change_conditions={Bpod.Events.Tup: 'Response_window2'},
            output_actions=[(Bpod.OutputChannels.LED, 2), (Bpod.OutputChannels.SoftCode, 13)]  # Incorrect sound
        )

        self.sma.add_state(
            state_name='Response_window2',
            state_timer=self.response_duration+10,
            state_change_conditions={'SoftCode1': 'Correct_other', 'SoftCode2': 'Incorrect',
                                     'SoftCode3': 'Miss', 'SoftCode4': 'Punish', Bpod.Events.Tup: 'Miss'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 5)]
        )
        self.sma.add_state(
            state_name='Correct_other',
            state_timer=0,
            state_change_conditions={Bpod.Events.Port1In: 'Correct_other_reward'},
            output_actions=[(Bpod.OutputChannels.PWM1, 5), (Bpod.OutputChannels.Valve, 2),
                            (Bpod.OutputChannels.SoftCode, 11)]
            # waterLED and correct sound remain ON until poke
        )
        self.sma.add_state(
            state_name='Correct_first_reward',
            state_timer=self.valve_time * self.valve_factor,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.Valve, 1), (Bpod.OutputChannels.SoftCode, 15)]
        )
        self.sma.add_state(
            state_name='Correct_other_reward',
            state_timer=self.valve_time * self.valve_factor2,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.Valve, 1), (Bpod.OutputChannels.SoftCode, 15)]
        )
        self.sma.add_state(
            state_name='Miss_reward',
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'Exit'},
            output_actions=[(Bpod.OutputChannels.SoftCode, 15)]
        )
        self.sma.add_state(
            state_name='Exit',  # Doors closure when trial ends
            state_timer=0,
            state_change_conditions={Bpod.Events.Tup: 'exit'},
            output_actions=[]
        )



    def after_trial(self):

        ############ TRIAL COUNTER ################

        ##### COUNT MISSES
        if self.current_trial_states['Miss'][0][0] > 0: # misses & incorrects modify the acc
            self.accwindow = self.accwindow[1:] + [0]
            if self.trial_type == 'VG':
                self.vg_accwindow = self.vg_accwindow[1:] + [0]
            elif self.trial_type == 'WM_I':
                self.wmi_accwindow = self.wmi_accwindow[1:] + [0]
            elif self.trial_type == 'WM_D':
                self.wmd_accwindow = self.wmd_accwindow[1:] + [0]

            if self.current_trial_states['Incorrect'][0][0] > 0:
                self.trial_result = 'incorrect'
                self.valid_counter += 1
            else:
                self.trial_result = 'miss'

        ##### COUNT PUNISH
        elif self.current_trial_states['Punish'][0][0] > 0:
            self.trial_result = 'punish'
            self.valid_counter += 1
            self.accwindow = self.accwindow[1:] + [0]
            if self.trial_type == 'VG':
                self.vg_accwindow = self.vg_accwindow[1:] + [0]
            elif self.trial_type == 'WM_I':
                self.wmi_accwindow = self.wmi_accwindow[1:] + [0]
            elif self.trial_type == 'WM_D':
                self.wmd_accwindow = self.wmd_accwindow[1:] + [0]

        ##### COUNT CORRECTS FIRST POKE
        elif self.current_trial_states['Correct_first'][0][0] > 0:
            self.trial_result = 'correct_first'
            self.valid_counter += 1
            self.reward_drunk += self.valve_reward * self.valve_factor
            self.accwindow = self.accwindow[1:] + [1]
            if self.trial_type == 'VG':
                self.vg_accwindow = self.vg_accwindow[1:] + [1]
            elif self.trial_type == 'WM_I':
                self.wmi_accwindow = self.wmi_accwindow[1:] + [1]
            elif self.trial_type == 'WM_D':
                self.wmd_accwindow = self.wmd_accwindow[1:] + [1]

        ##### COUNT CORRECTS OTHER POKE
        else:
            self.trial_result = 'correct_other'
            self.valid_counter += 1
            self.reward_drunk += self.valve_reward * self.valve_factor2
            self.accwindow = self.accwindow[1:] + [0]
            if self.trial_type == 'VG':
                self.vg_accwindow = self.vg_accwindow[1:] + [0]
            elif self.trial_type == 'WM_I':
                self.wmi_accwindow = self.wmi_accwindow[1:] + [0]
            elif self.trial_type == 'WM_D':
                self.wmd_accwindow = self.wmd_accwindow[1:] + [0]

        # End-trial calculations
        self.last_x = self.x
        self.trial_length = self.current_trial_states['Exit'][0][0] - self.current_trial_states['Start_task'][0][0]
        print('Trial lenght: ' + str(self.trial_length))

        ### Long trials
        if  self.current_trial >= 120 and self.trial_length > 30:
            self.tried_counter += 1
            if self.tried_counter > 2:
                self.tired = True
                print('Finishing task: subject tired (long trials)')
        else:  # reset the counter
            self.tried_counter = 0

        ############ REGISTER VALUES ################
        self.register_value('x', self.x)
        self.register_value('y', self.y)
        self.register_value('response_x', self.response_x)
        self.register_value('response_y', self.response_y)
        self.register_value('mask', self.mask)
        self.register_value('width', self.width)
        self.register_value('correct_th', self.correct_th)
        self.register_value('repoke_th', self.repoke_th)
        self.register_value('wm_stim_dur', self.wm_stim_dur)
        self.register_value('rw_stim_dur', self.rw_stim_dur)
        self.register_value('trial_type', self.trial_type)
        self.register_value('trial_type', self.trial_type)
        self.register_value('trial_result', self.trial_result)
        self.register_value('pvg', self.pvg)
        self.register_value('pwm_i', self.pwm_i)
        self.register_value('pwm_ds', self.pwm_ds)
        self.register_value('pwm_dm', self.pwm_dm)
        self.register_value('pwm_dl', self.pwm_dl)
        self.register_value('delay_type', self.delay_type)
        self.register_value('reward_drunk', self.reward_drunk)
        self.register_value('reponse_duration', self.response_duration)
        self.register_value('correction_bias', self.correction_bias)
        self.register_value('stim_duration', self.stim_duration)
        self.register_value('trial_length', self.trial_length)

