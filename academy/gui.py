from tkinter import *
import tkinter.ttk as ttk
import pandas as pd
from functools import partial
from academy import utils, settings, task_collection, functions


def close_window():
    tag = 'EXIT_ACADEMY'
    utils.queue_tags.put(tag)


def exit_gui():
    tag = 'EXIT_GUI'
    utils.queue_tags.put(tag)


class Gui(Frame):
    def __init__(self):
        self.window = Tk()
        self.window.title('ACADEMY box ' + str(settings.BOX_NAME))

        self.window.minsize(800, 800)
        self.window.protocol("WM_DELETE_WINDOW", close_window)
        self.state = 'waiting'
        self.name = ''
        self.subject = None
        self.subject_name = 'None'
        self.subject_var = StringVar()
        self.event_labels = []
        self.entries = []  # 0 is trials, 1 is time
        self.properties = []  # properties of the entries
        self.boolvars = []  # when the properties are bool
        self.blue = ttk.Style()
        self.blue.configure("blue.TButton", foreground="blue")
        self.red = ttk.Style()
        self.red.configure("red.TButton", foreground="red")
        Frame.__init__(self, self.window)
        self.draw()
        self.pack()

    def draw(self):

        margin = 5
        cell = 18
        three_cells = cell * 3
        all_no_margins = cell * 11
        row = 0

        # first row in 8 parts
        Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=1)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=2)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=3)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=4)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=5)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=6)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=7)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=8)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=9)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=10)
        Label(self, text='', anchor='w', width=cell).grid(row=row, column=11)
        Label(self, text='', anchor='w', width=margin).grid(row=row, column=12)
        row += 1

        if self.state == 'waiting':

            # tasks
            Label(self, text='TASKS:', anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)

            # exit gui
            ttk.Button(self, text='EXIT GUI', command=exit_gui, width=three_cells,
                       style='red.TButton').grid(row=row, column=9, columnspan=3, sticky='nsw')
            row += 1

            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1
            for task in task_collection.tasks:
                ttk.Button(self, text=task.task, command=partial(self.task_action, task.task), width=three_cells,
                           style='blue.TButton').grid(row=row, column=1, columnspan=3, sticky='nsw')
                row += 1

            # spaces
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # events
            Label(self, text='LAST EVENTS:', anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
            row += 1
            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1
            self.event_labels = []
            for i in range(20):
                label = Label(self, text='', anchor='w', width=all_no_margins)
                label.grid(row=row, column=1, columnspan=11)
                row += 1
                self.event_labels.append(label)

        if self.state == 'running_task':

            utils.state = 2

            # name of task
            Label(self, text=self.name, anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
            row += 1
            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1
            for _ in task_collection.tasks:
                Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
                row += 1

            # spaces
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # events
            Label(self, text='LAST EVENTS:', anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
            row += 1
            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1
            self.event_labels = []
            for i in range(20):
                label = Label(self, text='', anchor='w', width=all_no_margins)
                label.grid(row=row, column=1, columnspan=11)
                row += 1
                self.event_labels.append(label)

        elif self.state == 'setting_task':

            utils.state = 3

            run_task_flag = True

            self.entries = []
            self.boolvars = []

            # name of task
            Label(self, text=self.name, anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
            row += 1
            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1

            # space
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # task info
            if functions.task.info is not None:
                Label(self, text=str(functions.task.info), anchor='w',
                      width=all_no_margins).grid(row=row, column=1, columnspan=11)
                row += 1
                Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
                row += 1

            # subject
            possible_subjects = sorted(list({subject.name for subject in utils.subjects.items}))
            possible_subjects = ['None'] + possible_subjects

            Label(self, text='subject', anchor='e', width=cell).grid(row=row, column=1)
            combo = ttk.Combobox(self, width=15, textvariable=self.subject_var)
            combo['values'] = possible_subjects

            try:
                position = possible_subjects.index(functions.task.subject)
            except ValueError:
                position = 0
            combo.current(position)

            combo.grid(row=row, column=2, columnspan=3, sticky='nsw')
            row += 1

            # space
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # inputs
            all_inputs = functions.task.gui_input_fixed + functions.task.gui_input
            all_inputs_and_outputs = all_inputs + functions.task.gui_output
            all_inputs_and_outputs = pd.Series(all_inputs_and_outputs).drop_duplicates().tolist()

            for name in all_inputs_and_outputs:
                attribute = getattr(functions.task, name)
                if type(attribute) == bool:
                    # boolean variable
                    var = BooleanVar()
                    var.set(attribute)
                    Label(self, text=name, anchor='e', width=cell).grid(row=row, column=1)
                    check = Checkbutton(self, variable=var, width=cell)
                    if name not in all_inputs:
                        check.configure(state=DISABLED)
                    check.grid(row=row, column=2)
                    self.entries.append(check)
                    self.boolvars.append(var)
                    row += 1
                elif type(attribute) == list:
                    # list
                    if len(attribute) <= 10:
                        # numbers
                        for i in range(len(attribute)):
                            Label(self, text=str(i+1), anchor='w', width=margin).grid(row=row, column=i+2)
                        row += 1

                        # name list
                        Label(self, text=name, anchor='e', width=cell).grid(row=row, column=1)

                        if type(attribute[0]) == bool:
                            # boolean variables
                            for i in range(len(attribute)):
                                var = BooleanVar()
                                var.set(attribute[i])
                                check = Checkbutton(self, variable=var, width=cell)
                                if name not in all_inputs:
                                    check.configure(state=DISABLED)
                                check.grid(row=row, column=2 + i)
                                self.entries.append(check)
                                self.boolvars.append(var)
                            row += 1
                        else:
                            # float variables
                            for i in range(len(attribute)):
                                entry = Entry(self, width=cell)
                                entry.insert(0, attribute[i])
                                if name not in all_inputs:
                                    entry.configure(state=DISABLED)
                                entry.grid(row=row, column=2+i)
                                self.entries.append(entry)
                            row += 1
                    else:
                        # error
                        mytext = name + ' list is too large (maximum 10 values)'
                        Label(self, text=mytext, anchor='w',
                              width=all_no_margins).grid(row=row, column=1, columnspan=11)
                        run_task_flag = False
                        row += 1
                else:
                    # float variable
                    Label(self, text=name, anchor='e', width=cell).grid(row=row, column=1)
                    entry = Entry(self, width=cell)
                    entry.insert(0, attribute)
                    if name not in all_inputs:
                        entry.configure(state=DISABLED)
                    entry.grid(row=row, column=2)
                    self.entries.append(entry)
                    row += 1

                # space
                Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
                row += 1

            # spaces
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # run task
            if run_task_flag:
                ttk.Button(self, text='RUN TASK', command=self.run_task, width=three_cells,
                           style='blue.TButton').grid(row=row, column=2, columnspan=3, sticky='nsw')
                row += 1

            # space
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # cancel task
            ttk.Button(self, text='DONE', command=self.cancel, width=three_cells,
                       style='red.TButton').grid(row=row, column=2, columnspan=3, sticky='nsw')
            row += 1

            # space
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

        elif self.state == 'running_direct_task':

            utils.state = 3

            # name of task
            Label(self, text=self.name, anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
            row += 1
            ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=1, columnspan=11, sticky='ew')
            row += 1

            # spaces
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # warning
            if self.subject_name == 'None':
                Label(self, text='No subject selected for this task. No session data and no video will be saved.',
                      anchor='w', width=all_no_margins).grid(row=row, column=1, columnspan=11)
                row += 1

            # spaces
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

            # exit task
            ttk.Button(self, text='EXIT TASK', command=self.exit_task, width=three_cells,
                       style='red.TButton').grid(row=row, column=2, columnspan=3, sticky='nsw')
            row += 1

            # space
            Label(self, text='', anchor='w', width=margin).grid(row=row, column=0)
            row += 1

    def clear_frame(self):
        for widget in self.winfo_children():
            widget.destroy()

        self.pack_forget()

    def reload(self):
        if self.state == 'waiting' or self.state == 'running_task':
            last_events = utils.events.items[-20:]
            idx = 0
            for event in last_events:
                label = self.event_labels[idx]
                text = event.date + '     ' + str(event.description)
                if event.description.startswith('ERROR'):
                    label['fg'] = 'red'
                elif event.description.startswith('Running'):
                    label['fg'] = 'purple'
                elif event.description.startswith('---') or event.description.startswith('academy'):
                    label['fg'] = 'blue'
                elif event.description.startswith('Finishing'):
                    label['fg'] = 'green3'
                elif event.description.startswith('Connection to Bpod stablished'):
                    label['fg'] = 'green3'
                elif event.description.startswith('Waiting'):
                    label['fg'] = 'orange'
                else:
                    label['fg'] = 'gray'
                label['text'] = text
                idx += 1
        self.window.update()

    def task_action(self, name):
        for task in task_collection.tasks:
            if task.task == name:
                functions.task = task
                functions.touch.task = task
                functions.task.init_variables()
        self.name = name
        self.clear_and_draw('setting_task')

    def clear_and_draw(self, state):
        self.state = state
        self.clear_frame()
        Frame.__init__(self, self.window)
        self.draw()
        self.pack()
        self.reload()

    def run_task(self):

        if len(functions.task.gui_output) == 0:
            functions.task.init_variables()

        try:
            self.subject_name = self.subject_var.get()
            self.subject = utils.subjects.read_last_value('name', self.subject_name)
            i = 0
            self.properties = []
            for item in self.entries:
                if type(item) == Entry:
                    self.properties.append(float(item.get()))
                else:
                    self.properties.append(self.boolvars[i].get())
                    i += 1

            j = 0
            gui_input = functions.task.gui_input_fixed + functions.task.gui_input
            for name in gui_input:
                attribute = getattr(functions.task, name)
                if type(attribute) == list:
                    length = len(attribute)
                    values = self.properties[j:j+length]
                    setattr(functions.task, name, values)
                    j += length
                else:
                    setattr(functions.task, name, self.properties[j])
                    j += 1

        except ValueError:
            return

        functions.task.configure_gui()
        utils.queue_tags.put('RUN_DIRECT')
        self.clear_and_draw('running_direct_task')

    @staticmethod
    def exit_task():
        utils.queue_tags.put('EXIT_BUTTON')

    def close(self):
        self.pack_forget()
        self.window.destroy()
        # self.destroy()

    def cancel(self):
        utils.state = 0
        self.clear_and_draw('waiting')


class FakeWindow:
    def __init__(self):
        self.name = ''

    def update(self):
        pass


class FakeGui:
    def __init__(self):
        self.name = ''
        self.window = FakeWindow()

    def reload(self):
        pass

    def clear_and_draw(self, state):
        pass
