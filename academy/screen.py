from timeit import default_timer as timer
from academy import utils, functions
import queue


class Screen:
    def __init__(self):
        if hasattr(functions, 'window'):
            self.win = functions.window
        else:
            self.win = FakeWindow()

        self.tag = None
        self.exit_tag = None
        self.first = True
        self.start = timer()
        self.my_loop = lambda *args, **kwargs: None
        self.timing = 0

    def loop(self, task, gui):
        counter = 1
        self.tag = 'NOTHING'
        self.exit_tag = ''
        trials_performed = 0
        utils.start_timer()

        while trials_performed < task.trials_max and utils.check_timer() < task.duration_max and not task.tired:
            try:
                trials_performed = utils.queue_trials.get_nowait()
            except queue.Empty:
                trials_performed = 0
            counter += 1

            try:
                action = utils.queue_tags.get_nowait()
                if action == 'EXIT_BUTTON':
                    break
                elif action == 'GOING_OUT':
                    # utils.queue_tags.put('GOING_OUT')
                    break
            except queue.Empty:
                pass

            try:
                new_tag = utils.queue_softcode.get_nowait()
                self.tag = new_tag
                self.first = True
            except queue.Empty:
                pass

            self.play()
            gui.reload()

        utils.softcode.kill()
        self.clean()
        self.win.flip()
        task.tired = False

        self.tag = None
        self.first = False
        self.my_loop = lambda *args, **kwargs: None

    def play(self):

        if self.first:
            self.first = False

            if self.tag == 1:
                if hasattr(functions, 'function1'):
                    functions.function1()
                if hasattr(functions, 'loop1'):
                    self.start = timer()
                    self.my_loop = functions.loop1
            if self.tag == 2:
                if hasattr(functions, 'function2'):
                    functions.function2()
                if hasattr(functions, 'loop2'):
                    self.start = timer()
                    self.my_loop = functions.loop2
            if self.tag == 3:
                if hasattr(functions, 'function3'):
                    functions.function3()
                if hasattr(functions, 'loop3'):
                    self.start = timer()
                    self.my_loop = functions.loop3
            if self.tag == 4:
                if hasattr(functions, 'function4'):
                    functions.function4()
                if hasattr(functions, 'loop4'):
                    self.start = timer()
                    self.my_loop = functions.loop4
            if self.tag == 5:
                if hasattr(functions, 'function5'):
                    functions.function5()
                if hasattr(functions, 'loop5'):
                    self.start = timer()
                    self.my_loop = functions.loop5
            if self.tag == 6:
                if hasattr(functions, 'function6'):
                    functions.function6()
                if hasattr(functions, 'loop6'):
                    self.start = timer()
                    self.my_loop = functions.loop6
            if self.tag == 7:
                if hasattr(functions, 'function7'):
                    functions.function7()
                if hasattr(functions, 'loop7'):
                    self.start = timer()
                    self.my_loop = functions.loop7
            if self.tag == 8:
                if hasattr(functions, 'function8'):
                    functions.function8()
                if hasattr(functions, 'loop8'):
                    self.start = timer()
                    self.my_loop = functions.loop8
            if self.tag == 9:
                if hasattr(functions, 'function9'):
                    functions.function9()
                if hasattr(functions, 'loop9'):
                    self.start = timer()
                    self.my_loop = functions.loop9
            if self.tag == 10:
                if hasattr(functions, 'function10'):
                    functions.function10()
                if hasattr(functions, 'loop10'):
                    self.start = timer()
                    self.my_loop = functions.loop10
            if self.tag == 11:
                if hasattr(functions, 'function11'):
                    functions.function11()
                if hasattr(functions, 'loop11'):
                    self.start = timer()
                    self.my_loop = functions.loop11
            if self.tag == 12:
                if hasattr(functions, 'function12'):
                    functions.function12()
                if hasattr(functions, 'loop12'):
                    self.start = timer()
                    self.my_loop = functions.loop12
            if self.tag == 13:
                if hasattr(functions, 'function13'):
                    functions.function13()
                if hasattr(functions, 'loop13'):
                    self.start = timer()
                    self.my_loop = functions.loop13
            if self.tag == 14:
                if hasattr(functions, 'function14'):
                    functions.function14()
                if hasattr(functions, 'loop14'):
                    self.start = timer()
                    self.my_loop = functions.loop14
            if self.tag == 15:
                if hasattr(functions, 'function15'):
                    functions.function15()
                if hasattr(functions, 'loop15'):
                    self.start = timer()
                    self.my_loop = functions.loop15
            if self.tag == 16:
                if hasattr(functions, 'function16'):
                    functions.function16()
                if hasattr(functions, 'loop16'):
                    self.start = timer()
                    self.my_loop = functions.loop16
            if self.tag == 17:
                if hasattr(functions, 'function17'):
                    functions.function17()
                if hasattr(functions, 'loop17'):
                    self.start = timer()
                    self.my_loop = functions.loop17
            if self.tag == 18:
                if hasattr(functions, 'function18'):
                    functions.function18()
                if hasattr(functions, 'loop18'):
                    self.start = timer()
                    self.my_loop = functions.loop18
            if self.tag == 19:
                if hasattr(functions, 'function19'):
                    functions.function19()
                if hasattr(functions, 'loop19'):
                    self.start = timer()
                    self.my_loop = functions.loop19
            if self.tag == 20:
                if hasattr(functions, 'function20'):
                    functions.function20()
                if hasattr(functions, 'loop20'):
                    self.start = timer()
                    self.my_loop = functions.loop20

        self.timing = timer() - self.start
        self.my_loop(self.timing)

        # if self.tag == 1:
        #     if hasattr(functions, 'function1') and self.first:
        #         if functions.function1():
        #             self.current_loop = 1
        # elif self.tag == 2:
        #     if hasattr(functions, 'function2') and self.first:
        #         if functions.function2():
        #             self.current_loop = 2
        # elif self.tag == 3:
        #     if hasattr(functions, 'function3') and self.first:
        #         if functions.function3():
        #             self.current_loop = 3
        # elif self.tag == 4:
        #     if hasattr(functions, 'function4') and self.first:
        #         if functions.function4():
        #             self.current_loop = 4
        # elif self.tag == 5:
        #     if hasattr(functions, 'function5') and self.first:
        #         if functions.function5():
        #             self.current_loop = 5
        # elif self.tag == 6:
        #     if hasattr(functions, 'function6') and self.first:
        #         if functions.function6():
        #             self.current_loop = 6
        # elif self.tag == 7:
        #     if hasattr(functions, 'function7') and self.first:
        #         if functions.function7():
        #             self.current_loop = 7
        # elif self.tag == 8:
        #     if hasattr(functions, 'function8') and self.first:
        #         if functions.function8():
        #             self.current_loop = 8
        # elif self.tag == 9:
        #     if hasattr(functions, 'function9') and self.first:
        #         if functions.function9():
        #             self.current_loop = 9
        # elif self.tag == 10:
        #     if hasattr(functions, 'function10') and self.first:
        #         if functions.function10():
        #             self.current_loop = 10
        # if self.tag == 11:
        #     if hasattr(functions, 'function11') and self.first:
        #         if functions.function11():
        #             self.current_loop = 11
        # elif self.tag == 12:
        #     if hasattr(functions, 'function12') and self.first:
        #         if functions.function12():
        #             self.current_loop = 12
        # elif self.tag == 13:
        #     if hasattr(functions, 'function13') and self.first:
        #         if functions.function13():
        #             self.current_loop = 13
        # elif self.tag == 14:
        #     if hasattr(functions, 'function14') and self.first:
        #         if functions.function14():
        #             self.current_loop = 14
        # elif self.tag == 15:
        #     if hasattr(functions, 'function15') and self.first:
        #         if functions.function15():
        #             self.current_loop = 15
        # elif self.tag == 16:
        #     if hasattr(functions, 'function16') and self.first:
        #         if functions.function16():
        #             self.current_loop = 16
        # elif self.tag == 17:
        #     if hasattr(functions, 'function17') and self.first:
        #         if functions.function17():
        #             self.current_loop = 17
        # elif self.tag == 18:
        #     if hasattr(functions, 'function18') and self.first:
        #         if functions.function18():
        #             self.current_loop = 18
        # elif self.tag == 19:
        #     if hasattr(functions, 'function19') and self.first:
        #         if functions.function19():
        #             self.current_loop = 19
        # elif self.tag == 20:
        #     if hasattr(functions, 'function20') and self.first:
        #         if functions.function20():
        #             self.current_loop = 20
        #
        # if self.current_loop == 1:
        #     if hasattr(functions, 'loop1'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop1(timing)
        # elif self.current_loop == 2:
        #     if hasattr(functions, 'loop2'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop2(timing)
        # if self.current_loop == 3:
        #     if hasattr(functions, 'loop3'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop3(timing)
        # elif self.current_loop == 4:
        #     if hasattr(functions, 'loop4'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop4(timing)
        # if self.current_loop == 5:
        #     if hasattr(functions, 'loop5'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop5(timing)
        # elif self.current_loop == 6:
        #     if hasattr(functions, 'loop6'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop6(timing)
        # if self.current_loop == 7:
        #     if hasattr(functions, 'loop7'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop7(timing)
        # elif self.current_loop == 8:
        #     if hasattr(functions, 'loop8'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop8(timing)
        # elif self.current_loop == 9:
        #     if hasattr(functions, 'loop9'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop9(timing)
        # if self.current_loop == 10:
        #     if hasattr(functions, 'loop10'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop10(timing)
        # if self.current_loop == 11:
        #     if hasattr(functions, 'loop11'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop11(timing)
        # elif self.current_loop == 12:
        #     if hasattr(functions, 'loop12'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop12(timing)
        # if self.current_loop == 13:
        #     if hasattr(functions, 'loop13'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop13(timing)
        # elif self.current_loop == 14:
        #     if hasattr(functions, 'loop14'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop14(timing)
        # if self.current_loop == 15:
        #     if hasattr(functions, 'loop15'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop15(timing)
        # elif self.current_loop == 16:
        #     if hasattr(functions, 'loop16'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop16(timing)
        # if self.current_loop == 17:
        #     if hasattr(functions, 'loop17'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop17(timing)
        # elif self.current_loop == 18:
        #     if hasattr(functions, 'loop18'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop18(timing)
        # elif self.current_loop == 19:
        #     if hasattr(functions, 'loop19'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop19(timing)
        # elif self.current_loop == 20:
        #     if hasattr(functions, 'loop20'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop20(timing)






        # if self.tag == 1:
        #     if hasattr(functions, 'function1') and self.first:
        #         functions.function1()
        #     if hasattr(functions, 'loop1'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop1(timing)
        # elif self.tag == 2:
        #     if hasattr(functions, 'function2') and self.first:
        #         functions.function2()
        #     if hasattr(functions, 'loop2'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop2(timing)
        # elif self.tag == 3:
        #     if hasattr(functions, 'function3') and self.first:
        #         functions.function3()
        #     if hasattr(functions, 'loop3'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop3(timing)
        # elif self.tag == 4:
        #     if hasattr(functions, 'function4') and self.first:
        #         functions.function4()
        #     if hasattr(functions, 'loop4'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop4(timing)
        # elif self.tag == 5:
        #     if hasattr(functions, 'function5') and self.first:
        #         functions.function5()
        #     if hasattr(functions, 'loop5'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop5(timing)
        # elif self.tag == 6:
        #     if hasattr(functions, 'function6') and self.first:
        #         functions.function6()
        #     if hasattr(functions, 'loop6'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop6(timing)
        # elif self.tag == 7:
        #     if hasattr(functions, 'function7') and self.first:
        #         functions.function7()
        #     if hasattr(functions, 'loop7'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop7(timing)
        # elif self.tag == 8:
        #     if hasattr(functions, 'function8') and self.first:
        #         functions.function8()
        #     if hasattr(functions, 'loop8'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop8(timing)
        # elif self.tag == 9:
        #     if hasattr(functions, 'function9') and self.first:
        #         functions.function9()
        #     if hasattr(functions, 'loop9'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop9(timing)
        # elif self.tag == 10:
        #     if hasattr(functions, 'function10') and self.first:
        #         functions.function10()
        #     if hasattr(functions, 'loop10'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop10(timing)
        # if self.tag == 11:
        #     if hasattr(functions, 'function11') and self.first:
        #         functions.function11()
        #     if hasattr(functions, 'loop11'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop11(timing)
        # elif self.tag == 12:
        #     if hasattr(functions, 'function12') and self.first:
        #         functions.function12()
        #     if hasattr(functions, 'loop12'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop12(timing)
        # elif self.tag == 13:
        #     if hasattr(functions, 'function13') and self.first:
        #         functions.function13()
        #     if hasattr(functions, 'loop13'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop13(timing)
        # elif self.tag == 14:
        #     if hasattr(functions, 'function14') and self.first:
        #         functions.function14()
        #     if hasattr(functions, 'loop14'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop14(timing)
        # elif self.tag == 15:
        #     if hasattr(functions, 'function15') and self.first:
        #         functions.function15()
        #     if hasattr(functions, 'loop15'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop15(timing)
        # elif self.tag == 16:
        #     if hasattr(functions, 'function16') and self.first:
        #         functions.function16()
        #     if hasattr(functions, 'loop16'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop16(timing)
        # elif self.tag == 17:
        #     if hasattr(functions, 'function17') and self.first:
        #         functions.function17()
        #     if hasattr(functions, 'loop17'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop17(timing)
        # elif self.tag == 18:
        #     if hasattr(functions, 'function18') and self.first:
        #         functions.function18()
        #     if hasattr(functions, 'loop18'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop18(timing)
        # elif self.tag == 19:
        #     if hasattr(functions, 'function19') and self.first:
        #         functions.function19()
        #     if hasattr(functions, 'loop19'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop19(timing)
        # elif self.tag == 20:
        #     if hasattr(functions, 'function20') and self.first:
        #         functions.function20()
        #     if hasattr(functions, 'loop20'):
        #         if self.first:
        #             self.start = timer()
        #         timing = timer() - self.start
        #         functions.loop20(timing)

    def clean(self):
        pass


class FakeWindow:
    def __init__(self):
        self.name = 'fake'

    def flip(self):
        pass


screen = Screen()
