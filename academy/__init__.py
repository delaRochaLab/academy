import os
import psutil
from defaults_and_examples.create_user import create_user_from_defaults

create_user_from_defaults()

try:
    import user.settings as settings
    import user.collections as collections
    from academy.utils import utils
    utils.log('academy STARTED')
    import user.functions as functions
    import user.select_task as select_task
    from academy.screen import screen
    from academy.task_collection import task_collection
    from academy.touch_screen import touch
except ImportError:
    import defaults_and_examples.default_settings as settings
    raise


# killing all other python processes (similar to pkill python)
this_proc = os.getpid()

for proc in psutil.process_iter():
    procd = proc.as_dict(attrs=['pid', 'name'])
    if "python" in str(procd['name']) and procd['pid'] != this_proc:
        proc.kill()


# touchscreen
os.system(settings.XINPUT)
