import evdev
import os
import time
#from academy import functions
from select import select
from datetime import datetime
from threading import Thread
import numpy as np
from numpy import linalg as ln
from academy import utils, settings


class Timer:

    def __init__(self, timeup):
        self.timeup = timeup  # in seconds
        self.initial_time = datetime.now()

    def reset(self):
        self.initial_time = datetime.now()

    @property
    def remaining_time(self):
        elapsed_time = datetime.now() - self.initial_time
        seconds = self.timeup - elapsed_time.total_seconds()
        if seconds < 0:
            return 0
        else:
            return seconds


class Touch:

    def __init__(self, touch_device, only_x, first_touch, win_resolution, touch_resolution, response_queue):

        self.touch_device = touch_device
        self.win_resolution = win_resolution
        self.touch_resolution = touch_resolution
        self.response_queue = response_queue

        self.task = None

        self.only_x = only_x
        self.first_touch = first_touch

        self.timer = None

        self.time_between_responses = 0.5

        self.device = evdev.InputDevice(touch_device)
        self.device.grab()

    def close(self):
        self.device.ungrab()

    def start_reading(self, duration, x, y, correct_th, repoke_th):
        self.timer = Timer(duration)
        t = Thread(target=self.run, args=(x, y, correct_th, repoke_th,), daemon=True)
        t.start()

    def resume_reading(self, x, y, correct_th, repoke_th):
        t = Thread(target=self.run, args=(x, y, correct_th, repoke_th,), daemon=True)
        t.start()

    def create_new_device(self):
        i = 0.001
        error_flag = True
        while i < 10:
            i *= 10
            try:
                os.system(settings.XINPUT)
                self.device.ungrab()
                self.device = evdev.InputDevice(self.touch_device)
                self.device.grab()
                i = 10
                utils.log('new device created')
                error_flag = False
            except:
                utils.log('touchscreen not found, waiting', i, 'ms')
                time.sleep(i)
        if error_flag:
            self.task.tired = True

    def run(self, x, y, correct_th, repoke_th):
        x_coord = None
        y_coord = None
        answer = None

        try:
            while self.device.read_one() is not None:  # clearing buffer of events
                pass
        except:
            utils.log('lectureError in touchscreen clearing buffer, creating new device')
            self.create_new_device()

        while self.timer.remaining_time > 0:
            event = None
            try:
                event = self.device.read_one()
            except:
                utils.log('lectureError in touchscreen, creating new device')
                self.create_new_device()

            if event is not None:
                if event.type == evdev.ecodes.EV_ABS:  # if event is a coordinate
                    if event.code == 0 or event.code == 53:  # x coord
                        x_coord = event.value
                    if event.code == 1 or event.code == 54:  # y coord
                        y_coord = event.value
                    if self.first_touch and x_coord is not None and (y_coord is not None or self.only_x):
                        answer = [x_coord, y_coord]
                        break
                elif event.type == evdev.ecodes.EV_KEY and event.value != 1:  # BTN_TOUCH up
                    if not self.first_touch and x_coord is not None and (y_coord is not None or self.only_x):
                        answer = [x_coord, y_coord]
                        break

        if answer is None:
            utils.softcode.send(3)
            response = []

        else:
            xpsy = abs(x)
            ypsy = abs(y)

            xtouch = abs(answer[0] * (self.win_resolution[0] / self.touch_resolution[0]))
            try:
                ytouch = abs(answer[1] * (self.win_resolution[1] / self.touch_resolution[1]))
            except:
                ytouch = None

            if self.only_x:
                if abs(xtouch - xpsy) < correct_th / 2:
                    utils.softcode.send(1)
                elif abs(xtouch - xpsy) < repoke_th / 2:
                    utils.softcode.send(2)
                else:
                    utils.softcode.send(4)
            else:
                if ln.norm(np.array((xtouch, ytouch)) - np.array((xpsy, ypsy))) < correct_th / 2:
                    utils.softcode.send(1)
                elif ln.norm(np.array((xtouch, ytouch)) - np.array((xpsy, ypsy))) < repoke_th / 2:
                    utils.softcode.send(2)
                else:
                    utils.softcode.send(4)

            if ytouch is None:
                ytouch = 0

            response = [xtouch / settings.PIXELS_PER_MM, ytouch / settings.PIXELS_PER_MM]

        self.response_queue.put(response)


touch = Touch(settings.TOUCHSCREEN_PORT, True, True, settings.WIN_RESOLUTION,
              settings.TOUCH_RESOLUTION, utils.response_queue)
