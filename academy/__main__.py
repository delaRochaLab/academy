import os
import sys
import time
from academy import functions
from academy import utils
from academy.gui import Gui, FakeGui
from academy.task_collection import task_collection
from pybpodapi.protocol import Bpod


def main():

    try:
        gui = Gui()

        try:
            from academy.camera import cam2
        except ImportError:
            utils.log('academy EXIT')
            utils.log('---------------------')
            gui.reload()
            time.sleep(2)
            exit_app()

        try:
            from academy.rfid import rfid
        except ImportError:
            rfid = None
            utils.log('academy EXIT')
            utils.log('---------------------')
            gui.reload()
            time.sleep(2)
            exit_app()

        if not task_collection.tasks:
            utils.log('ERROR: Tasks not found at directory tasks/')
            utils.log('academy EXIT')
            utils.log('---------------------')
            gui.reload()
            time.sleep(2)
            exit_app()

        if not connection_with_bpod(gui):
            utils.log('ERROR: Connection to Bpod failed')
            utils.log('academy EXIT')
            utils.log('---------------------')
            gui.reload()
            time.sleep(2)
            exit_app()

        # cam2 = None

        rfid.start_reading_tags()
        while True:
            if utils.state == 0:
                gui.clear_and_draw('waiting')
                utils.log('Waiting')
            if utils.state == 1:
                gui.clear_and_draw('waiting')
                utils.log('Subject in tunnel')
            elif utils.state == 2:
                utils.log('Waiting for subject to leave')
            gui.reload()
            while utils.queue_tags.empty():
                gui.window.update()

            tag = utils.queue_tags.get()

            if tag == 'EXIT_ACADEMY':
                if utils.state == 0:
                    utils.log('academy EXIT')
                    utils.log('---------------------')
                    exit_app()
                else:
                    utils.log('There is a subject inside the box, wait until it leaves to exit academy')
            elif tag == 'EXIT_GUI':
                utils.log('Closing the GUI')
                gui.close()
                gui = FakeGui()
            elif tag == 'RELOAD':
                pass
            elif tag == 'RUN_DIRECT':
                task_collection.run_direct_task(subject=gui.subject, gui=gui)
            else:
                subject = utils.subjects.read_last_value('tag', tag)
                task_collection.run_task(subject=subject, gui=gui)

    except KeyboardInterrupt:
        print('')
        utils.log('academy EXIT')
        utils.log('---------------------')
        exit_app()


def exit_app():
    f = open(os.devnull, 'w')
    sys.stdout = f
    sys.stderr = f
    sys.tracebacklimit = 0
    try:
        for attrib in dir(functions):
            delattr(functions, attrib)
    except AttributeError:
        pass
    sys.exit(0)


def connection_with_bpod(gui):
    utils.log('Stablishing connection with Bpod...')
    gui.reload()
    i = 0
    connection = False
    while i < 4:
        try:
            bpod = Bpod()
            bpod.close()
            i = 4
            connection = True
            utils.log('Connection to Bpod stablished')
        except BaseException:
            time.sleep(1)
            i += 1
    return connection


if __name__ == "__main__":
    main()
