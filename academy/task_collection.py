import inspect
import os
import pkgutil
import hashlib
import time
from shutil import copyfile
from threading import Thread
from academy import settings, utils
from academy.screen import screen
from academy.task_manager import TaskManager
from academy.camera import create_cam
from academy import functions
from pybpodapi.protocol import Bpod, StateMachine
import queue


def softcode_handler(data):
    utils.queue_softcode.put(data)


class Task(object):

    def __init__(self):
        self.task = 'UNKNOWN'
        self.p = None
        self.stage = 0
        self.checksum = None
        self.subject_class = None
        self.subject = None
        self.subject_weight = 0
        self.my_bpod = None
        self.sma = None
        self.current_trial_states = None
        self.box = settings.BOX_NAME
        self.date = None
        self.current_trial = 0
        self.trials_min = settings.DEFAULT_TRIALS_MIN
        self.duration_min = settings.DEFAULT_DURATION_MIN
        self.trials_max = settings.DEFAULT_TRIALS_MAX
        self.duration_max = settings.DEFAULT_DURATION_MAX
        self.tired = False
        self.gui_input_fixed = ['subject_weight']
        self.gui_input = []
        self.gui_output = []
        self.info = None
        self.collection = None
        self.values_dict = {}
        self.response_x = []
        self.response_y = []

        self.q = utils.queue_softcode
        self.loop = self.clear

    def clear(self):
        pass

    def init_variables(self):
        pass

    def set_and_run(self, gui, subject, subject_name, subject_weight, task_manager, direct):
        self.subject_class = subject
        self.subject = subject_name
        self.subject_weight = subject_weight

        # empty response_queue (from previous task)
        while True:
            try:
                utils.response_queue.get_nowait()
            except queue.Empty:
                break

        # empty softcode_queue (from previous task)
        while True:
            try:
                utils.queue_softcode.get_nowait()
            except queue.Empty:
                break

        self.current_trial = 0
        trials = int(self.trials_max)

        if task_manager is not None:

            self.date = task_manager.start[:10]
            video_directory = os.path.join(settings.VIDEOS_DIRECTORY, subject_name)

            if not os.path.exists(video_directory):
                os.mkdir(video_directory)
            functions.cam2 = create_cam(settings.CAMERA2_PORT, video_directory, task_manager.filename)
            functions.cam2.play()
            functions.cam2.record()
        else:
            self.date = utils.now()[:10]

        self.p = Thread(target=self.run, args=(trials,), daemon=True)
        self.p.start()
        screen.loop(self, gui)
        self.p.join()
        try:
            self.my_bpod.close()
        except AttributeError:
            pass
        self.my_bpod = None

        if direct:
            gui.clear_and_draw('setting_task')
        else:
            gui.clear_and_draw('waiting')

        try:
            functions.cam2.stop()
        except (AttributeError, AssertionError):
            pass

    def run(self, trials):
        utils.start_timer()
        if self.my_bpod is None:
            try:
                self.my_bpod = Bpod()
            except UnicodeDecodeError:
                time.sleep(1)
                utils.log('waiting for bpod ready')
                self.my_bpod = Bpod()
            self.my_bpod.softcode_handler_function = softcode_handler

            while True:
                utils.queue_trials.put(self.current_trial)
                utils.current_trials = self.current_trial
                self.sma = StateMachine(self.my_bpod)
                if self.current_trial < trials:
                    self.main_loop()
                else:
                    self.sma.add_state(
                        state_name='End',
                        state_timer=100,
                        state_change_conditions={Bpod.Events.Tup: 'exit'},
                        output_actions=[]
                    )
                if len(self.sma.state_names) == 0:
                    self.sma.add_state(
                        state_name='End',
                        state_timer=0,
                        state_change_conditions={Bpod.Events.Tup: 'exit'},
                        output_actions=[]
                    )
                self.my_bpod.send_state_machine(self.sma)
                self.my_bpod.run_state_machine(self.sma)
                self.current_trial_states = self.my_bpod.session.current_trial.states_durations
                self.update_response()
                self.after_trial()
                self.register_values()
                self.current_trial += 1

    def main_loop(self):
        raise NotImplementedError

    def after_trial(self):
        pass

    def configure_gui(self):
        pass

    def update_response(self):
        self.response_x = []
        self.response_y = []
        response_list = []
        while True:
            try:
                response_list.append(utils.response_queue.get_nowait())
            except queue.Empty:
                break
        for item in response_list:
            try:
                self.response_x.append(item[0])
                self.response_y.append(item[1])
            except IndexError:
                pass

        self.response_x = ','.join(str(e) for e in self.response_x)
        self.response_y = ','.join(str(e) for e in self.response_y)

    def register_value(self, key, value):
        self.my_bpod.register_value(key, value)

    def register_values(self):
        self.my_bpod.register_value('task', self.task)
        self.my_bpod.register_value('stage', self.stage)
        self.my_bpod.register_value('checksum', self.checksum)
        self.my_bpod.register_value('subject', self.subject)
        self.my_bpod.register_value('subject_weight', self.subject_weight)
        self.my_bpod.register_value('box', self.box)
        self.my_bpod.register_value('date', self.date)

        for i in range(len(functions.task.gui_input)):
            name = functions.task.gui_input[i]
            attribute = getattr(functions.task, name)
            self.my_bpod.register_value(name, attribute)

        self.my_bpod.register_value('TRIAL', None)


class TaskCollection(object):
    def __init__(self):
        self.tasks_package = 'tasks'
        self.tasks = []
        self.seen_paths = []
        self.reload_tasks()

    def reload_tasks(self):
        self.tasks = []
        self.seen_paths = []
        self.walk_package(self.tasks_package)
        for task in self.tasks:
            task.init_variables()
            task.configure_gui()

    @staticmethod
    def run_direct_task(subject, gui):

        if subject is None:
            subject_name = 'None'
            task_manager = None
        else:
            subject_name = subject.name
            subject.task = functions.task.task
            subject.stage = functions.task.stage
            task_manager = TaskManager(subject)

        t = 'Running task: ' + functions.task.task + '  /  Subject: ' + subject_name
        utils.log(t)

        utils.queue_tags.put('RUN_TASK')
        gui.reload()

        functions.task.set_and_run(gui, subject, subject_name, functions.task.subject_weight, task_manager, True)

        if task_manager is not None:
            task_manager.save_csvs()
        t = 'Finishing' + t[7:]
        utils.log(t)

    def run_task(self, subject, gui):
        found = False

        subject_name = subject.name
        subject_weight = subject.weight

        for task in self.tasks:
            if task.task == subject.task:
                
                functions.task = task
                functions.touch.task = task
                functions.task.init_variables()
                functions.task.stage = subject.stage
                task_manager = TaskManager(subject)

                t = 'Running task: ' + functions.task.task + '  /  Subject: ' + subject_name
                utils.log(t)
                gui.name = t
                gui.clear_and_draw('running_task')

                functions.task.set_and_run(gui, subject, subject_name, subject_weight, task_manager, False)

                task_manager.save_csvs()
                t = 'Finishing' + t[7:]
                utils.log(t)
                found = True
                break

        if not found:
            utils.log('ERROR: task:', subject.task, 'not found for subject:', subject_name)

    def walk_package(self, package):
        try:
            imported_package = __import__(package, fromlist=['blah'])
        except ModuleNotFoundError:
            return

        for _, pluginname, ispkg in pkgutil.iter_modules(imported_package.__path__, imported_package.__name__ + '.'):
            if not ispkg:
                plugin_module = __import__(pluginname, fromlist=['blah'])
                clsmembers = inspect.getmembers(plugin_module, inspect.isclass)
                for (_, c) in clsmembers:
                    if issubclass(c, Task) & (c is not Task):
                        path = inspect.getmodule(c().__class__).__file__
                        name = c.__name__
                        utils.log('Loading task:', name, 'from:', path)
                        new_task = c()
                        new_task.task = name
                        self.tasks.append(new_task)
                        self.create_checksum(name, path)

        all_current_paths = []
        if isinstance(imported_package.__path__, str):
            all_current_paths.append(imported_package.__path__)
        else:
            all_current_paths.extend([x for x in imported_package.__path__])

        for pkg_path in all_current_paths:
            if pkg_path not in self.seen_paths:
                self.seen_paths.append(pkg_path)

                child_pkgs = [p for p in os.listdir(pkg_path) if os.path.isdir(os.path.join(pkg_path, p))]

                for child_pkg in child_pkgs:
                    self.walk_package(package + '.' + child_pkg)

    def create_checksum(self, name, path):
        filename = os.path.splitext(os.path.basename(path))[0]
        digester = hashlib.md5()
        with open(path, 'rb') as f:
            for chunk in iter(lambda: f.read(4096), b""):
                digester.update(chunk)
        checksum = digester.hexdigest()
        new_path = os.path.join(settings.BACKUP_TASKS_DIRECTORY, filename + '_' + checksum + '.py')
        for task in self.tasks:
            if task.task == name:
                task.checksum = checksum
        if not os.path.exists(new_path):
            copyfile(path, new_path)

    @staticmethod
    def path_generator(path, pattern):
        paths = []
        for root, _, file in os.walk(path):
            for f in file:
                if f.endswith(pattern):
                    paths.append(os.path.join(root, f))
        return paths


task_collection = TaskCollection()
