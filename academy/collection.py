import pandas as pd
import numpy as np
import os
import csv
from academy.utils import utils
from academy import settings


class Item(object):
    def __init__(self, iterable=(), **kwargs):
        self.__dict__.update(iterable, **kwargs)


class Collection:
    def __init__(self, name, default_dict=None):
        self.items = []
        self.name = name
        self.path = os.path.join(settings.DATA_DIRECTORY, name + '.csv')
        self.read_from_csv(default_dict)

    def as_dict(self):
        my_dict = {}
        for k in self.items[0].__dict__.keys():
            my_dict[k] = list(np.concatenate(list([item.__dict__[k]] for item in self.items)))
        return my_dict

    def as_df(self):
        my_dict = self.as_dict()
        return pd.DataFrame(my_dict)

    def save_csv(self):
        my_df = self.as_df()
        my_df.to_csv(self.path, index=False, sep=';')

    def read_from_csv(self, default_dict):
        self.items = []
        try:
            my_df = pd.read_csv(self.path, sep=';')
            if default_dict is not None:
                for name in default_dict.keys():
                    if name not in list(my_df):
                        my_df[name] = np.nan
        except FileNotFoundError:
            new_dict = {'date': utils.now(), **default_dict, }
            my_df = pd.DataFrame(new_dict, index=[0])
            my_df.to_csv(self.path, index=False, sep=';')
        my_list_of_dicts = my_df.to_dict(orient='records')
        for my_dict in my_list_of_dicts:
            item = Item(my_dict)
            self.items.append(item)

    def add_new_item(self, item_dict, item=None):
        if item_dict.get('filename') == '':
            pass
        else:
            if self.items:
                if item is not None:
                    new_dict = item.__dict__
                    for key, value in item_dict.items():
                        new_dict[key] = value
                else:
                    new_dict = item_dict

                if 'date' in new_dict:
                    new_dict['date'] = utils.now()
                else:
                    new_dict = {'date': utils.now(), **new_dict}


                new_item = Item(new_dict)
                self.items.append(new_item)
                with open(self.path, 'a') as f:
                    writer = csv.writer(f, delimiter=';')
                    writer.writerow(new_item.__dict__.values())
                if self.name != 'events':
                    utils.log('Element added to collection:', self.name)
            else:
                new_dict = {'date': utils.now(), **item_dict}
                new_item = Item(new_dict)
                self.items.append(new_item)
                self.save_csv()

    def read_first_value(self, column1, value):
        try:
            result = [item for item in self.items if item.__dict__[column1] == value][0]
        except (KeyError, IndexError):
            result = None
        return result

    def read_last_value(self, column1, value):
        try:
            result = [item for item in self.items if item.__dict__[column1] == value][-1]
        except (KeyError, IndexError):
            result = None
        return result
