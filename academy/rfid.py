import serial
import time
from datetime import timedelta
from threading import Thread
from academy import settings, utils, functions


class Rfid:
    def __init__(self):
        self.last_tag = [{}, {}, {}]
        self.min_time = 10  # seconds, min time from other animal detection

        self.t = None
        self.arduino = serial.Serial(settings.ARDUINO_SERIAL_PORT, 9600, timeout=1)
        self.ecohab = serial.Serial(settings.ECOHAB_SERIAL_PORT, 115200, timeout=1)

    def enough_time(self, tag, antenna):
        now = utils.time_now()
        read_time = timedelta(seconds=1000)
        for i in range(len(self.last_tag)):
            for key, value in self.last_tag[i].items():
                if key != tag or i == antenna - 1:
                    read_time = min(read_time, now - value)
        return read_time > timedelta(seconds=self.min_time)

    def start_reading_tags(self):
        self.t = Thread(target=self.read_tag, daemon=True)
        self.t.start()

    def read_tag(self):
        while True:
            tag_message = 'RELOAD'
            message = self.ecohab.readline()
            tags = message.decode('utf-8')

            if tags:
                tag = tags[:10]
                antenna = int(tags[11])
                if antenna == 5:  # we are using 1, 5, 3
                    antenna = 2

                if self.enough_time(tag, antenna):
                    self.last_tag[antenna - 1][tag] = utils.time_now()
                    subject = utils.subjects.read_last_value('tag', tag)

                    if subject is None:
                        utils.log('ERROR: subject not found for tag:', tag, ' / Antenna:', antenna)

                    else:
                        utils.log('Tag read:', tag, ' / Subject:', subject.name, ' / Antenna:', antenna)

                        if utils.state == 0 and antenna == 2:
                            min_time = utils.string_to_time(subject.date, subject.wait_seconds)
                            if utils.time_now() > min_time:
                                utils.state = 1
                                utils.log('Closing door1, opening door2, subject going from home to tunnel')
                                self.arduino.write(b'0')
                            else:
                                utils.log('Subject not allowed to enter until:', utils.time_to_string(min_time))

                        elif utils.state == 1 and antenna == 2:
                            utils.state = 0
                            utils.log('Closing door2, opening door1, subject going from tunnel to home')
                            self.arduino.write(b'1')

                        elif utils.state == 1 and antenna == 3:
                            utils.state = 2
                            utils.log('Closing door2, subject in box')
                            self.arduino.write(b'2')
                            tag_message = tag

                        elif utils.state == 2 and antenna == 3:
                            min_time = functions.task.duration_min
                            min_trials = functions.task.trials_min
                            if utils.check_timer() > min_time and utils.current_trials >= min_trials:
                                utils.log('Opening door2, subject going from box to tunnel')
                                utils.state = 1
                                self.arduino.write(b'3')
                                tag_message = 'GOING_OUT'
                            elif utils.check_timer() > min_time:
                                utils.log('Subject not allowed to exit until a minimum number of trials:', min_trials)
                            else:
                                utils.log('Subject not allowed to exit until a minimum duration of:', min_time)

                else:
                    utils.log('Other subject detected in antenna')

                utils.queue_tags.put(tag_message)

                time.sleep(1)
                message = self.ecohab.readline()
                while message:
                    message = self.ecohab.readline()


class FakeRfid:
    def __init__(self):
        self.task_on = False
        self.t = None

    def start_reading_tags(self):
        pass


try:
    rfid = Rfid()
except (ValueError, serial.SerialException):
    utils.log('ERROR: arduino or ecohab not found')
    rfid = FakeRfid()
