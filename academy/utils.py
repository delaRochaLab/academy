from datetime import datetime, timedelta
from timeit import default_timer as timer
from queue import Queue
from academy import settings
from toolsR.utils.SoftcodeConnection import SoftcodeConnection


class Utils:
    def __init__(self):

        self.queue_tags = Queue()
        self.queue_softcode = Queue()
        self.queue_trials = Queue()
        self.response_queue = Queue()
        self.current_trials = 0
        self.state = 0  # 0 outside, 1 tunnel, 2 manual run, 3 direct run
        self.response = ''
        self.softcode = SoftcodeConnection(address=settings.PYBPOD_NET_PORT)
        self.init_time = 0
        self.events = None

    def log(self, *args):
        date = self.now()
        description = ' '.join(map(str, args))
        print(date + '   ' + description)
        event_dict = {'description': description}
        self.events.add_new_item(event_dict)

    @staticmethod
    def now():
        return datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    @staticmethod
    def now_for_files():
        return datetime.now().strftime("%Y%m%d-%H%M%S")

    @staticmethod
    def time_now():
        return datetime.now()

    @staticmethod
    def time_to_string(time):
        return time.strftime("%Y/%m/%d %H:%M:%S")

    @staticmethod
    def string_to_time(time_string, delay):
        time0 = datetime.strptime(time_string, "%Y/%m/%d %H:%M:%S")
        time1 = timedelta(seconds=delay)
        return time0 + time1

    def start_timer(self):
        self.init_time = timer()

    def check_timer(self):
        return timer() - self.init_time


utils = Utils()
