from academy import settings
from academy.utils import utils


class FakeCam:

    def __init__(self):
        self.name = 'fakecam'
        self.fake = True

    def play(self):
        pass

    def record(self):
        pass

    def put_state(self, something):
        pass

    def stop(self):
        pass


def create_cam(port, path, title):
    try:
        from toolsR import VideoR
        date = utils.now_for_files()

        cam_sync = {"Correct": (600, 30), "Incorrect": (600, 70), "Punish": (600, 100), "Miss": (600, 130),
                    "Resp Window": (600, 130)}

        return VideoR(indx_or_path=port,
                      name_video=title + '_' + date + '.avi',
                      path=path + '/',
                      title=title,
                      fps=settings.FPS,
                      codec_cam=settings.CODEC,
                      codec_video=settings.CODEC,
                      cam_sync=cam_sync,
                      states_on_sync=True)

    except (ImportError, BaseException):
        utils.log('ERROR: ' + title + ' not found')
        return FakeCam()


cam2 = create_cam(settings.CAMERA2_PORT, settings.VIDEOS_DIRECTORY, settings.VIDEO_TITLE2)
