# THIS IS A BACKUP FILE. DO NOT CHANGE IT.
# THE FIRST TIME THE APPLICATION IS LAUNCHED, A COPY OF THIS FILE IS CREATED IN THE USER DIRECTORY.
# ALWAYS MODIFY THE WORKING VERSION IN THE USER DIRECTORY.


import pkg_resources
import os

# default bpod values (not to be changed)
TARGET_BPOD_FIRMWARE_VERSION = "22"
PYBPOD_BAUDRATE = 1312500
PYBPOD_SYNC_CHANNEL = 255
PYBPOD_SYNC_MODE = 1
PYBPOD_API_MODULES = []
SESSION_NAME = 'session'
DISTRIBUTION_DIRECTORY = pkg_resources.get_distribution('academy').location
TASKS_DIRECTORY = os.path.join(DISTRIBUTION_DIRECTORY, 'tasks')
USER_DIRECTORY = os.path.join(DISTRIBUTION_DIRECTORY, 'user')
DATA_DIRECTORY = os.path.join(DISTRIBUTION_DIRECTORY, 'data')
BACKUP_TASKS_DIRECTORY = os.path.join(DATA_DIRECTORY, 'backup_tasks')
SESSIONS_DIRECTORY = os.path.join(DATA_DIRECTORY, 'sessions')
VIDEOS_DIRECTORY = os.path.join(DATA_DIRECTORY, 'videos')


# serial and net ports
ARDUINO_SERIAL_PORT = '/dev/ttyUSB-Arduino'
ECOHAB_SERIAL_PORT = '/dev/ttyUSB-Ecohab'
PYBPOD_SERIAL_PORT = '/dev/ttyACM-Bpod2'
PYBPOD_NET_PORT = 36000  # network port to receive remote commands like softcodes
TOUCHSCREEN_PORT = '/dev/input/by-id/usb-Touch__KiT_Touch_Computer_INC.-event-if00'
CAMERA2_PORT = "/dev/video-Cam2"


# bpod ports
BPOD_BNC_PORTS_ENABLED = [False, False]
BPOD_WIRED_PORTS_ENABLED = [False, False]
BPOD_BEHAVIOR_PORTS_ENABLED = [True, True, True, True, False, False, False, False]  # ports that are activated
BPOD_BEHAVIOR_PORTS_WATER = [True, False, False, False, False, False, False, False]  # ports that deliver water


# touchscreen
XINPUT = 'xinput map-to-output "Touch__KiT Touch  Computer INC." DP-3'
WIN_SIZE = [403, 252]  # in mm
WIN_RESOLUTION = [1440, 900]
TOUCH_RESOLUTION = [4096, 4096]
SCREEN_NUMBER = 1
VIEW_POSITION = [-int(WIN_RESOLUTION[0] / 2), -int(WIN_RESOLUTION[1] / 2)]
WIN_COLOR = [-1, -1, -1]
PIXELS_PER_MM = 3.57
STIM_WIDTH = 40  # mm


# camera
VIDEO_TITLE2 = 'Cam2'
CODEC = 'X264'
FPS = 120


# other
BOX_NAME = 2
DEFAULT_TRIALS_MIN = 0
DEFAULT_DURATION_MIN = 0  # seconds
DEFAULT_TRIALS_MAX = 1000
DEFAULT_DURATION_MAX = 36000  # seconds
