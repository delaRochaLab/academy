#include <Stepper.h>
#define STEPS 2048


Stepper stepper(STEPS, 8, 10, 9, 11);
Stepper stepper2(STEPS, 4, 6, 5, 7);


void closeDoor1()
{
      stepper2.step(250);
      delay(10);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
      digitalWrite(7, LOW);
}

void openDoor2()
{
      stepper.step(250);
      delay(10);
      digitalWrite(8, LOW);
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);
}

void openDoor1()
{
      stepper2.step(-250);
      delay(10);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
      digitalWrite(7, LOW);
}

void closeDoor2()
{
      stepper.step(-250);
      delay(10);
      digitalWrite(8, LOW);
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);
}



void setup()
{
  Serial.begin(9600);
  stepper.setSpeed(10);
  stepper2.setSpeed(10);
}



void serialEvent()
{
  while(Serial.available())
  {
    char ch = Serial.read();
    Serial.flush();

    if (ch == '0')
    {
      closeDoor1();
      openDoor2();
    }
    else if (ch == '1')
    {
      closeDoor2();
      openDoor1();
    }
    else if (ch == '2')
    {
      closeDoor2();
    }
    else if (ch == '3')
    {
      openDoor2();
    }
  }
}



void loop()
{

}