#include <LiquidCrystal.h>
#include <Stepper.h>
#include <HX711.h>
#include <Wire.h>
#define number_of_subjects  22

LiquidCrystal lcd(6, 7, 8, 9, 10, 11);
HX711 LoadCell;
char tag[22];
String tags_array[number_of_subjects] = {"041A9D86C5", "0417CA97FA", "0419A81BFB", "041A9D88DE", "041A9DBEC0", "041A9DBD90", "0419A8701C", "0419A87060", "0417CA5FDE", "0419A8218D", "041A9DA41D", "000C0BCF3F", "041A9CA74C", "0419A86B51", "0419A8692F", "041A9DBDF9", "041A9DB9F6", "0419A86ECB", "041A9D6C0A", "000C0BCE80", "041A9DBBE0", "041A9D6AB4"};
String subjects_array[number_of_subjects] = {"MA6", "MA8", "MA10", "MA12", "MA14", "A14", "A15", "A16", "A17", "A18", "A19", "A20", "A31", "A32", "A33", "A34", "A35", "A36", "A37", "A38", "A39", "A40"};


void fetchTagData1(char tempTag[])
{
  Serial1.read();

  for (int counter = 0; counter < 10; counter++)
  {
    tempTag[counter] = Serial1.read();
  }

  Serial1.read();
  Serial1.read();
  Serial1.read();
  Serial1.read();
}

void printTag(char tag[])
{
  lcd.clear();
  lcd.setCursor(0, 0);
  for (int counter = 0; counter < 10; counter++)
  {
    lcd.print(tag[counter]);
  }

  int position = -1;
  for (int counter = 0; counter < number_of_subjects; counter++)
  {
    for (int counter2 = 0; counter2 < 10; counter2++)
    {
      Serial.print(counter);
      Serial.print(counter2);
      Serial.print(tag[counter2]);
      Serial.print(" ");
      if (tag[counter2] == tags_array[counter].charAt(counter2)) {
        position = counter;
      }
      else 
      {
        position = -1;
        Serial.print("   ");
        break;
      }
    }
    if (position >= 0)
    {
      break;
    }
  }

  if (position >= 0) {
    //Serial.print(position);
    lcd.setCursor(11, 0);
    int length_name = subjects_array[position].length();
    for (int counter = 0; counter < length_name; counter++)
    {
      lcd.print(subjects_array[position].charAt(counter));
    }
  }
}



void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  lcd.begin(16, 2);
  LoadCell.begin(2, 3); // start connection to HX711
  LoadCell.set_scale(1062); // calibration factor for load cell => strongly dependent on your individual setup 
  LoadCell.tare();
}



void loop()
{
    
  if (Serial1.available() > 0)
  {
    delay(30);
    if (Serial1.peek() != 2)
    {
      while(Serial1.available())
      {
        Serial1.read();
      }
    }
    else
    {
      fetchTagData1(tag);
      while(Serial.available())
      {
        Serial.read();
      }
      printTag(tag);

      LoadCell.tare();

      while(Serial.available())
      {
        Serial.read();
      }
    }
  }
  
  float result = LoadCell.get_units(5);
  lcd.setCursor(0, 1);
  lcd.print(result);
  delay(1000);
 
}
