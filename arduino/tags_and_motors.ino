#include <Stepper.h>
#define STEPS 2048


Stepper stepper(STEPS, 8, 10, 9, 11);
Stepper stepper2(STEPS, 4, 6, 5, 7);
char tag[10];


void flushSerialBuffer1()
{
  while (Serial1.available() > 0)
  {
    Serial.read();
  }
}

void flushSerialBuffer2()
{
  while (Serial2.available() > 0)
  {
    Serial.read();
  }
}

void flushSerialBuffer3()
{
  while (Serial3.available() > 0)
  {
    Serial.read();
  }
}



void fetchTagData1(char tempTag[])
{
  Serial1.read();

  for (int counter = 0; counter < 10; counter++)
  {
    tempTag[counter] = Serial1.read();
  }

  Serial1.read();
  Serial1.read();
  Serial1.read();
  Serial1.read();

  if (Serial1.read() != 3)
  {
    for (int counter = 0; counter < 10; counter++)
    {
      tempTag[counter] = '0';
    }
  }
  else
  {
    flushSerialBuffer1();
  }
}

void fetchTagData2(char tempTag[])
{
  Serial2.read();

  for (int counter = 0; counter < 10; counter++)
  {
    tempTag[counter] = Serial2.read();
  }

  Serial2.read();
  Serial2.read();
  Serial2.read();
  Serial2.read();

  if (Serial2.read() != 3)
  {
    for (int counter = 0; counter < 10; counter++)
    {
      tempTag[counter] = '0';
    }
  }
  else
  {
    flushSerialBuffer2();
  }
}

void fetchTagData3(char tempTag[])
{
  Serial3.read();

  for (int counter = 0; counter < 10; counter++)
  {
    tempTag[counter] = Serial3.read();
  }

  Serial3.read();
  Serial3.read();
  Serial3.read();
  Serial3.read();

  if (Serial3.read() != 3)
  {
    for (int counter = 0; counter < 10; counter++)
    {
      tempTag[counter] = '0';
    }
  }
  else
  {
    flushSerialBuffer3();
  }
}



void printTag(char tag[])
{
  for (int counter = 0; counter < 10; counter++)
  {
    Serial.print(tag[counter]);
  }
}



void moveMotorInExp()
{
      stepper.step(-250);
      delay(10);
      digitalWrite(8, LOW);
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);

      //stepper2.step(250);
      //delay(10);
      //digitalWrite(4, LOW);
      //digitalWrite(5, LOW);
      //digitalWrite(6, LOW);
      //digitalWrite(7, LOW);
}

void moveMotorOutExp()
{
      stepper.step(250);
      delay(10);
      digitalWrite(8, LOW);
      digitalWrite(9, LOW);
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);

      //stepper2.step(-250);
      //delay(10);
      //digitalWrite(4, LOW);
      //digitalWrite(5, LOW);
      //digitalWrite(6, LOW);
      //digitalWrite(7, LOW);
}



void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);

  stepper.setSpeed(10);
  stepper2.setSpeed(10);
}



void serialEvent()
{
  while(Serial.available())
  {
    char ch = Serial.read();
    Serial.flush();

    if (ch == '0')
    {
      moveMotorOutExp();
    }
    else if (ch == '1')
    {
      moveMotorInExp();
    }
  }
}



void loop()
{
  if (Serial1.available() > 0)
  {
    delay(250);
    if (Serial2.peek() != 2)
    {
      flushSerialBuffer1();
    }
    else
    {
      fetchTagData1(tag);
      Serial.flush();
      printTag(tag);
      Serial.print(1);
    }
  }

  else if (Serial2.available() > 0)
  {
    delay(250);
    if (Serial2.peek() != 2)
    {
      flushSerialBuffer2();
    }
    else
    {
      fetchTagData2(tag);
      Serial.flush();
      printTag(tag);
      Serial.print(2);
    }
  }

  else if (Serial3.available() > 0)
  {
    delay(250);
    if (Serial2.peek() != 2)
    {
      flushSerialBuffer3();
    }
    else
    {
      fetchTagData3(tag);
      Serial.flush();
      printTag(tag);
      Serial.print(3);
    }
  }

  delay(1000);
}
