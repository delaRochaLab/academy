from academy.collection import Collection
from academy.utils import utils


events = {'description': 'Creating new events.csv file'
          }

subjects = {'name': 'animal0',
            'tag': 'unknown',
            'weight': '0',
            'task': 'SimpleScreen',
            'stage': 0,
            'wait_seconds': 0
            }

water_calibration = {'port': 1,
                     'water': 8,
                     'pulse_duration': 0.01,
                     }

utils.events = Collection('events', events)
utils.subjects = Collection('subjects', subjects)
utils.water_calibration = Collection('water_calibration', water_calibration)
