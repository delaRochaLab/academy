import os
from psychopy import visual, logging
from academy import settings
from academy.touch_screen import touch
from academy.camera import cam2


# init global variable task
task = None
cam2 = cam2

# to avoid the recurrent psychopy monitor warning
logging.console.setLevel(logging.CRITICAL)

touch.task = task

# create the window
window = visual.Window(size=settings.WIN_RESOLUTION, screen=settings.SCREEN_NUMBER, color=settings.WIN_COLOR,
                       units='pix', fullscr=False, viewPos=settings.VIEW_POSITION)
os.system('wmctrl -r "PsychoPy" -b add,above')

# create all the stimuli you will use in all tasks
square = visual.Rect(win=window,
                     height=settings.WIN_RESOLUTION[1],
                     width=int(settings.STIM_WIDTH * settings.PIXELS_PER_MM),
                     units='pix',
                     lineColor=[0.2, 0.2, 0.2],
                     fillColor=[0.2, 0.2, 0.2],
                     pos=(int(settings.WIN_RESOLUTION[0] / 2), int(settings.WIN_RESOLUTION[1] / 2)))

gray_screen = visual.Rect(win=window,
                          width=settings.WIN_RESOLUTION[0],
                          height=settings.WIN_RESOLUTION[1],
                          units='pix',
                          fillColor=[1, 1, 1],
                          pos=(int(settings.WIN_RESOLUTION[0] / 2), int(settings.WIN_RESOLUTION[1] / 2)))


# when softcode n is called, function n runs once
# then loop n runs until another softcode is called




# PSYCHOPY SOFTCODES

# draw a square with task.x, task.y, task.width and task.stim_duration
def function1():
    square.pos = (int(task.x * settings.PIXELS_PER_MM), int(task.y * settings.PIXELS_PER_MM))
    square.width = int(task.width * settings.PIXELS_PER_MM)

def loop1(timing):
    if timing < task.stim_duration:
        square.draw()
        # if task.blinking != 0 and task.stim_duration > 5:
        #     rounded_timing = int(timing)
        #     if rounded_timing % 2 == 0:
        #         square.draw()
        #     else:
        #         pass
        # else:
        #     square.draw()
    window.flip()

def function2():
    square.pos = (int(task.x * settings.PIXELS_PER_MM), int(task.y * settings.PIXELS_PER_MM))
    square.width = int(task.width * settings.PIXELS_PER_MM)

def loop2(timing):
    square.draw()
    # if task.blinking != 0:
    #     rounded_timing = int(timing)
    #     if rounded_timing % 2 == 0:
    #         square.draw()
    #     else:
    #         pass
    # else:
    #     square.draw()
    window.flip()

# TOUCHSCREEN SOFTCODES

# start reading
def function4():
    touch.start_reading(task.response_duration, task.x * settings.PIXELS_PER_MM, task.y * settings.PIXELS_PER_MM,
                        task.correct_th * settings.PIXELS_PER_MM, task.repoke_th * settings.PIXELS_PER_MM)
    cam2.put_state('Resp Window')


# resume reading
def function5():
    touch.resume_reading(task.x * settings.PIXELS_PER_MM, task.y * settings.PIXELS_PER_MM,
                        task.correct_th * settings.PIXELS_PER_MM, task.repoke_th * settings.PIXELS_PER_MM)
    cam2.put_state('Resp Window')



# CAMERA (AND PSYCHOPY) SOFTCODES

# camera correct and delete screen
def function11():
    cam2.put_state('Correct')
def loop11(timing):
    window.flip()


# camera miss with grey screen
def function12():
    cam2.put_state('Miss')
def loop12(timing):
    # gray_screen.draw()
    window.flip()


# camera incorrect
def function13():
    cam2.put_state('Incorrect')


# camera incorrect outside theshold with grey screen
def function14():
    cam2.put_state('Punish')
def loop14(timing):
    # gray_screen.draw()
    window.flip()


# camera empty and delete screen
def function15():
    cam2.put_state('')
def loop15(timing):
    window.flip()

