# Examples of functions to calculate new task and stage
# If the function fails to return, new task and stage will be previous task and previous stage
# df is the session dataframe for the subject


# Same task and stage
def select_task(df, subject):

    task = subject.task
    stage = subject.stage
    wait_seconds = 30  # wait 30 seconds minimum for the new session

    return task, stage, wait_seconds


# # If performance > 70% in the last 100 trials go to next task
# def select_task(df, subject):
#
#     task = subject.task
#     stage = subject.stage
#     wait_seconds = 60
#
#     df = df.iloc[-100:]  # last 100 trials
#
#     if df['correct'].count() > 70:  # more than 70 corrects
#
#         if task == 'Task1':
#             task = 'Task2'
#         elif task == 'Task2':
#             task = 'Task3'
#
#     return task, stage, wait_seconds
